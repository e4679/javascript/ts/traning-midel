export function parseWord(input: string): string[];
export function parseWord(input: string[]): string;

export function parseWord (input: unknown): unknown {
  if(Array.isArray(input)) {
    return input.join('');
  }

  if(typeof input === 'string') {
    return [...input];
  }
}

const first = parseWord('EDGAR');
const second = parseWord(first);
const third = parseWord(second);

console.log(first);
console.log(second);
console.log(third);