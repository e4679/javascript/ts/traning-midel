// Array tipado
const prices: (number | string)[] = [12, 3, 4, 'aasdasd'];

// Tupla solo acepta 2 valores el primero debe ser string y el segundo number
let myUser: [string, number];
myUser = ['emeneses', 12];
// No es asignable a la tupla myUser
// myUser = ['123', 12, 1231, 123]
