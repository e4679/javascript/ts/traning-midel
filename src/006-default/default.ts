export const createProduct = (
  id: string | number,
  isNew: boolean = true,
  stock: number = 10,
) => ({
  id,
  stock: stock,
  isNew: isNew
});

const product1 = createProduct('12312', true, 2);
const product2 = createProduct('22312', false, 0);
const product3 = createProduct('32312');


console.log(product1);
console.log(product2);
console.log(product3);