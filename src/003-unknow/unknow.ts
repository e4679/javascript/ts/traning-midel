let unknowVar: unknown;

unknowVar = 122;
unknowVar = '123123123';
unknowVar = true;
unknowVar = {}

if(typeof unknowVar === 'string') {
  unknowVar.toUpperCase();
}

const newVal: boolean = (typeof unknowVar === 'boolean') && unknowVar;

console.log(newVal);