import { User, ROLES } from "../001-enums/enums";

const currentUser: User = {
  usename: 'emeneses',
  role: ROLES.CUSTOMER
};

export const isAdmin = (user: User) => user.role === ROLES.ADMIN

export const checkRole = (user: User, role1: ROLES, role2: ROLES) => (user.role === role1 || user.role === role2);

export const checkRoleV2 = (user: User, roles: ROLES[]) => roles.includes(user.role);

export const checkRoleV3 = (user: User, ...roles: ROLES[]) => roles.includes(user.role);

console.log(isAdmin(currentUser));
console.log(checkRole(currentUser, ROLES.ADMIN, ROLES.SELLER));
console.log(checkRoleV2(currentUser, [ROLES.ADMIN, ROLES.SELLER]));
console.log(checkRoleV3(currentUser, ROLES.ADMIN, ROLES.SELLER, ROLES.CUSTOMER));


