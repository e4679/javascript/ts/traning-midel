export enum ROLES {
  ADMIN = 'admin',
  SELLER = 'seller',
  CUSTOMER = 'customer'
}

export type User = {
  usename: string,
  role: ROLES
}

const user: User = {
  usename: 'emeneses',
  role: ROLES.ADMIN
}
