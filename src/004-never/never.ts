const fail = (message: string) => {
  throw new Error(message);
}

const validateType = (input: unknown) => {
  if(typeof input === 'string') {
    return 'It is string';
  }

  if(Array.isArray(input)) {
    return 'It is array'
  }
  return fail('Not match');
}

console.log(validateType('hola'));
console.log(validateType([12,12]));
console.log(validateType(12));

