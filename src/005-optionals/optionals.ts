export const createProduct = (
  id: string | number,
  isNew?: boolean,
  stock?: number,
) => ({
  id,
  stock: stock ?? 10,
  isNew: isNew ?? true
});

const product1 = createProduct('12312', true, 2);
const product2 = createProduct('12312', false, 0);

console.log(product1);
console.log(product2);