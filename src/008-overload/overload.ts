// El metodo debe hacer:
// recibe por parametro un string y retorna un array de string EJ EDGAR ==> [E, D, G, A, R]
// recibe un array de string y retorna un string EJ [E, D, G, A, R] ==> EDGAR

/* const toArray = (word: string) => {
  return [...word];
}

const toStringV2 = (word: string[]) => {
  return word.join().replace(',', '');
}

console.log(toArray('EDGAR'));

console.log(toStringV2(['E', 'D', 'G', 'A', 'R'])); */

function parseWord (input: string | string[]): string | string[] {
  return (Array.isArray(input)) ? input.join('') : [...input];
}

const first = parseWord('EDGAR') as string[];
const second = parseWord(first) as string;
const third = parseWord(second);

console.log(first);
console.log(second);
console.log(third);